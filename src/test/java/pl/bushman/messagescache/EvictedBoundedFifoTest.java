package pl.bushman.messagescache;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.*;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EvictedBoundedFifoTest {
    private static final int SIZE = 100;

    private ExecutorService executorService = Executors.newFixedThreadPool(100);

    @Test
    public void isBoundedAndEvictsAndIsFifo() throws InterruptedException, ExecutionException {
        EvictedBoundedFifo<String> evictedBoundedFifo = new EvictedBoundedFifo(SIZE);

        int elementCount = 80;
        int overridingElementCount = 40;
        addElements(evictedBoundedFifo, 1, elementCount, "first");
        addElements(evictedBoundedFifo, 1, overridingElementCount, "second");

        List<String> elements = evictedBoundedFifo.stream().collect(Collectors.toList());

        assertEquals(SIZE, elements.size());
        assertEquals(SIZE - overridingElementCount
                , elements.stream().filter(el -> el.equals("first")).count());
        assertEquals(overridingElementCount
                , elements.stream().filter(el -> el.equals("second")).count());
    }

    @Test
    public void isThreadSafe() throws ExecutionException, InterruptedException {
        EvictedBoundedFifo<String> evictedBoundedFifo = new EvictedBoundedFifo(SIZE);

        int writerCount = 50;
        int elementsPerWrite = 50;

        List<Callable<Boolean>> writers = writers(evictedBoundedFifo, writerCount, elementsPerWrite, "el");
        List<Callable<Boolean>> readers = readers(evictedBoundedFifo, 50, 50);

        List<Future<Boolean>> writerFutures = startWorkers(writers);
        List<Future<Boolean>> readerFutures = startWorkers(readers);

        waitUntilFinished(writerFutures);
        waitUntilFinished(readerFutures);

        assertEquals(writerCount * elementsPerWrite, evictedBoundedFifo.getPosition());

    }


    private void addElements(EvictedBoundedFifo<String> evictedBoundedFifo, int writers, int elementsPerWrite, String element) throws InterruptedException, ExecutionException {
        waitUntilFinished(
                startWorkers(
                        writers(evictedBoundedFifo, writers, elementsPerWrite, element)));
    }


    private List<Callable<Boolean>> readers(EvictedBoundedFifo<String> evictedBoundedFifo, int readers, int readsPerReader) {
        return prepareWorkers(readers, readsPerReader, i -> evictedBoundedFifo.stream().collect(Collectors.toList()));
    }

    private List<Callable<Boolean>> writers(EvictedBoundedFifo<String> evictedBoundedFifo, int writers, int elementsPerWrite, String element) {
        return prepareWorkers(writers, elementsPerWrite, i -> evictedBoundedFifo.add(element));
    }

    private List<Callable<Boolean>> prepareWorkers(int workers, int actionsPerWorker, IntConsumer consumer) {
        Callable<Boolean> workerCallable = () -> {
            IntStream.range(0, actionsPerWorker)
                    .forEach(consumer);
            return true;
        };
        return IntStream.range(0, workers)
                .mapToObj(i -> workerCallable)
                .collect(Collectors.toList());
    }

    private List<Future<Boolean>> startWorkers(List<Callable<Boolean>> workers) throws InterruptedException {
        return executorService.invokeAll(workers);
    }

    private void waitUntilFinished(List<Future<Boolean>> workerFutures) throws ExecutionException, InterruptedException {
        for (Future<Boolean> future : workerFutures) {
            future.get();
        }
    }


}
