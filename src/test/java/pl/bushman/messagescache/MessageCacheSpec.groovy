package pl.bushman.messagescache

import spock.lang.Specification

import java.time.LocalDateTime
import java.util.stream.Collectors
import java.util.stream.IntStream

class MessageCacheSpec extends Specification {

    def dateProvider = Spy(new DateProvider())
    def messageCache = new MessageCache(dateProvider)

    def "returns only 100 last messages"() {
        given: "add message to be replaced by newer one later"
        messageCache.add(new Message("first", -1))

        and:
        def _100Messages = prepare100Messages()
        _100Messages.stream()
                .forEach(messageCache::add)

        when:
        def resultMessages = messageCache.recentMessages()

        then:
        resultMessages as Set == _100Messages
    }

    def "returns only recent messages "() {
        given: "first message is added '10 minutes ago'"
        dateProvider.getLocalDateTime() >>> [LocalDateTime.now().minusMinutes(10), LocalDateTime.now()]

        and:
        def oldMessage = new Message("old", 1)
        messageCache.add(oldMessage)

        def recentMessage = new Message("recent", 2)
        messageCache.add(recentMessage)

        when:
        def resultMessages = messageCache.recentMessages()

        then:
        resultMessages.contains(recentMessage)
        !resultMessages.contains(oldMessage)
    }

    Set<Message> prepare100Messages() {
        IntStream.range(0, 100)
                .mapToObj(i -> new Message("agent_" + 1, i))
                .collect(Collectors.toSet())
    }
}
