package pl.bushman.messagescache;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MessageCacheTest {

    private static final int SUCCESS_HTTP_CODE = 200;
    private static final int ERROR_HTTP_CODE = 404;

    @Test
    public void returns100MostRecentMessages() {
        MessageCache messageCache = new MessageCache(new DateProvider());

        int messagesCount = 70;
        int overridingMessagesCount = 35;
        addMessages(messageCache, messagesCount, "first", SUCCESS_HTTP_CODE);
        addMessages(messageCache, overridingMessagesCount, "second", SUCCESS_HTTP_CODE);

        List<Message> messages = messageCache.recentMessages();

        assertEquals(MessageCache.SIZE, messages.size());
        assertEquals(MessageCache.SIZE-overridingMessagesCount
                , messages.stream().filter(msg -> msg.agent.equals("first")).count());
        assertEquals(overridingMessagesCount
                , messages.stream().filter(msg -> msg.agent.equals("second")).count());

    }

    @Test
    public void doesntReturnOutDatedMessages() {
        DateProvider dateProvider = mock(DateProvider.class);
        LocalDateTime outDatedTime = LocalDateTime.now().minusMinutes(15);
        when(dateProvider.getLocalDateTime()).thenReturn(outDatedTime, LocalDateTime.now());

        MessageCache messageCache = new MessageCache(dateProvider);
        addMessages(messageCache, 1, "first", SUCCESS_HTTP_CODE);

        List<Message> messages = messageCache.recentMessages();

        assertEquals(0, messages.size());
    }

    @Test
    public void returnsErrorMessagesCount() {
        MessageCache messageCache = new MessageCache(new DateProvider());

        int errorMessagesCount = 20;
        addMessages(messageCache, 10, "first", SUCCESS_HTTP_CODE);
        addMessages(messageCache, errorMessagesCount, "second", ERROR_HTTP_CODE);
        addMessages(messageCache, 10, "first", SUCCESS_HTTP_CODE);

        long count = messageCache.recentErrorMessagesCount();

        assertEquals(errorMessagesCount, count);
    }

    private void addMessages(MessageCache messageCache, int messagesCount, String agent, int httpCode) {
        IntStream.range(0, messagesCount)
                .forEach(i -> messageCache.add(new Message(agent, httpCode)));
    }


}
