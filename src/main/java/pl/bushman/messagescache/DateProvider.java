package pl.bushman.messagescache;

import java.time.LocalDateTime;

public class DateProvider {

    public LocalDateTime getLocalDateTime() {
        return LocalDateTime.now();
    }
}
