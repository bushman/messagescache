package pl.bushman.messagescache;

import java.util.Arrays;
import java.util.concurrent.locks.StampedLock;
import java.util.stream.Stream;

public class EvictedBoundedFifo<E> {
    private final StampedLock lock = new StampedLock();
    private final Object[] buffer;
    private int position = 0;

    public EvictedBoundedFifo(int size) {
        buffer = new Object[size];
    }

    public void add(E e) {
        long stamp = lock.writeLock();
        try {
            buffer[position % buffer.length] = e;
            position++;
        } finally {
            lock.unlock(stamp);
        }
    }

    public Stream<E> stream() {
        return Arrays.stream(safeBufferCopy(), 0, Math.min(100, position))
                .map(obj -> (E) obj);
    }

    private Object[] safeBufferCopy() {
        Object[] bufferCopy;
        long stamp = lock.tryOptimisticRead();
        bufferCopy = Arrays.copyOf(buffer, buffer.length);
        if (!lock.validate(stamp)) {
            stamp = lock.readLock();
            try {
                bufferCopy = Arrays.copyOf(buffer, buffer.length);
            } finally {
                lock.unlock(stamp);
            }
        }
        return bufferCopy;
    }

    int getPosition() {
        return position;
    }
}
