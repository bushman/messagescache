package pl.bushman.messagescache;

import java.util.List;
import java.util.stream.Collectors;

public class MessageCache {

    static final int SIZE = 100;

    private final DateProvider dateProvider;
    private final EvictedBoundedFifo<TimestampedMessage> messages = new EvictedBoundedFifo<>(SIZE);

    public MessageCache(DateProvider dateProvider) {
        this.dateProvider = dateProvider;
    }

    public void add(Message message) {
        messages.add(new TimestampedMessage(message, dateProvider.getLocalDateTime()));
    }

    public List<Message> recentMessages() {
        return messages.stream()
                .filter(msg -> !msg.isOutdated(dateProvider))
                .map(msg -> msg.message)
                .collect(Collectors.toList());
    }

    public long recentErrorMessagesCount() {
        return messages.stream()
                .filter(msg -> !msg.isOutdated(dateProvider))
                .filter(TimestampedMessage::isError)
                .count();
    }

}
