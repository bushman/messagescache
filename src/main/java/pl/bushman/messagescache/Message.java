package pl.bushman.messagescache;

public class Message {
    final String agent;
    final int httpCode;

    public Message(String agent, int httpCode) {
        this.agent = agent;
        this.httpCode = httpCode;
    }
}
