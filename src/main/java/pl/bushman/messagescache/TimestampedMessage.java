package pl.bushman.messagescache;

import java.time.LocalDateTime;

class TimestampedMessage {
    private static final int TIME_TO_LIVE_MINUTES = 5;

    final Message message;
    private final LocalDateTime localDateTime;

    public TimestampedMessage(Message message, LocalDateTime localDateTime) {
        this.message = message;
        this.localDateTime = localDateTime;
    }

    public boolean isOutdated(DateProvider dateProvider) {
        return localDateTime.plusMinutes(TIME_TO_LIVE_MINUTES).isBefore(dateProvider.getLocalDateTime());
    }

    public boolean isError() {
        return message.httpCode >= 400;
    }
}
